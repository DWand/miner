function Game(fieldJqueryWrapper, tileSize) {
    this.newGame = newGame;

    var fieldDom = fieldJqueryWrapper;
    var TILE_SIZE = tileSize;
    var CLASS_WIN = 'miner-game-win';
    var CLASS_LOSE = 'miner-game-lose';
    var CLASS_FLAG_CLICKED = 'flag-clicked';

    var width;
    var height;
    var tilesCount;
    var minesCount;

    var tiles = [];

    function newGame(fieldWidth, fieldHeight, fieldMinesCount) {
        width = fieldWidth;
        height = fieldHeight;
        tilesCount = width * height;
        minesCount = fieldMinesCount;

        generateField();
        generateDom();
    }

    function generateField() {
        tiles = [];
        for (var i = 0; i < tilesCount; i++) {
            tiles.push({
                minesCount: 0,
                isRevealed: false,
                hasMine: false,
                hasFlag: false,
                dom: undefined
            });
        }

        for (var i = 0; i < minesCount; i++) {
            var mineIndex = getRandomFreeTileIndex();
            tiles[mineIndex].hasMine = true;

            var affectedTileIndexes = getSurroundingTileIndexes(mineIndex);
            for (var j = 0, len = affectedTileIndexes.length; j < len; j++) {
                tiles[affectedTileIndexes[j]].minesCount += 1;
            }
        }
    }

    function generateDom() {
        fieldDom.removeClass(CLASS_WIN);
        fieldDom.removeClass(CLASS_LOSE);
        fieldDom.empty();
        fieldDom.css({
            width: TILE_SIZE * width,
            height: TILE_SIZE * height
        });

        for (var i = 0; i < tilesCount; i++) {
            var tileDom = createTileDom(i);

            fieldDom.append(tileDom);
            tiles[i].dom = tileDom;
        }
    }

    function createTileDom(tileIndex) {
        var tile = tiles[tileIndex],
            tileDom = $('<div>');

        tileDom.addClass('tile').css({
            width: TILE_SIZE + 'px',
            height: TILE_SIZE + 'px',
            lineHeight: TILE_SIZE + 'px'
        });

        if (tile.hasMine) {
            tileDom.addClass('mine');
        } else {
            tileDom.addClass('mines-counter-' + tile.minesCount);
        }

        tileDom.data('tileIndex', tileIndex);
        tileDom.on('click', onTileClick);
        tileDom.on('contextmenu', onTileFlag);

        return tileDom;
    }

    function onTileClick(event) {
        var tileDom = $(event.target),
            tileIndex = tileDom.data('tileIndex'),
            tile = tiles[tileIndex];

        if (tile.hasFlag) {
            onFlagClick(tileDom);
            return;
        }

        tile.isRevealed = true;
        updateRevealedTileDom(tileIndex);

        if (!tile.hasMine && tile.minesCount === 0) {
            revealNearestTiles(tileIndex);
        }

        if (tile.hasMine) {
            finishGameLose();
        } else if (!hasFreeUnrevealedTiles()) {
            finishGameWin();
        }
    }

    function onTileFlag(event) {
        var tileDom = $(event.target),
            tileIndex = tileDom.data('tileIndex'),
            tile = tiles[tileIndex];

        tile.hasFlag = !tile.hasFlag;
        if (tile.hasFlag) {
            tileDom.addClass('flag');
        } else {
            tileDom.removeClass('flag');
        }

        return false;
    }

    function onFlagClick(tileDom) {
        var timeout = tileDom.data('flagClickTimeout');
        if (timeout) {
            return;
        }

        timeout = setTimeout(function() {
            tileDom.removeClass(CLASS_FLAG_CLICKED);
            tileDom.data('flagClickTimeout', null);
        }, 500);

        tileDom.addClass(CLASS_FLAG_CLICKED);
        tileDom.data('flagClickTimeout', timeout);
    }

    function revealNearestTiles(tileIndex) {
        var stack = getSurroundingTileIndexes(tileIndex).filter(filterUnrevealedTiles),
            tile, nearestTileIndexes;

        while (stack.length > 0) {
            tileIndex = stack.pop();
            tile = tiles[tileIndex];

            if (tile.isRevealed) {
                continue;
            }

            tile.isRevealed = true;
            updateRevealedTileDom(tileIndex);

            if (tile.minesCount > 0) {
                continue;
            }

            nearestTileIndexes = getSurroundingTileIndexes(tileIndex).filter(filterUnrevealedTiles);
            Array.prototype.push.apply(stack, nearestTileIndexes);
        }
    }

    function filterUnrevealedTiles(tileIndex) {
        return !tiles[tileIndex].isRevealed;
    }

    function updateRevealedTileDom(tileIndex) {
        var tile = tiles[tileIndex],
            tileDom = tile.dom;

        tileDom.addClass('revealed');
        tileDom.removeClass('flag');
        if (!tile.hasMine) {
            tileDom.text(tile.minesCount || '');
        }

        tileDom.off('click', onTileClick);
        tileDom.off('contextmenu', onTileFlag);
    }

    function getRandomFreeTileIndex() {
        while (true) {
            var index = getRandomInt(0, tilesCount - 1);
            if (!tiles[index].hasMine) {
                return index;
            }
        }
    }

    function hasFreeUnrevealedTiles() {
        for (var i = 0; i < tilesCount; i++) {
            var tile = tiles[i];

            if (!tile.hasMine && !tile.isRevealed) {
                return true;
            }
        }

        return false;
    }

    function finishGameLose() {
        finalizeGame(Game.EVENT_LOSE, CLASS_LOSE);
    }

    function finishGameWin() {
        finalizeGame(Game.EVENT_WIN, CLASS_WIN);
    }

    function finalizeGame(event, fieldClassName) {
        fieldDom.addClass(fieldClassName);
        fieldDom.children().off('click', onTileClick);
        fieldDom.trigger(event);
    }

    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function getSurroundingTileIndexes(tileIndex) {
        var row = Math.floor(tileIndex / width),
            col = tileIndex % width,
            hasPaddingTop = row > 0,
            hasPaddingRight = col < width - 1,
            hasPaddingBottom = row < height - 1,
            hasPaddingLeft = col > 0,
            indexes = [];

        if (hasPaddingLeft) {
            indexes.push(tileIndex - 1);

            if (hasPaddingTop) {
                indexes.push(tileIndex - width - 1);
            }

            if (hasPaddingBottom) {
                indexes.push(tileIndex + width - 1);
            }
        }

        if (hasPaddingRight) {
            indexes.push(tileIndex + 1);

            if (hasPaddingTop) {
                indexes.push(tileIndex - width + 1);
            }

            if (hasPaddingBottom) {
                indexes.push(tileIndex + width + 1);
            }
        }

        if (hasPaddingTop) {
            indexes.push(tileIndex - width);
        }

        if (hasPaddingBottom) {
            indexes.push(tileIndex + width);
        }

        return indexes;
    }
}

Game.EVENT_WIN = 'miner:win';
Game.EVENT_LOSE = 'miner:lose';

$(function() {
    var field = $('#field'),
        widthFld = $('#widthFld'),
        heightFld = $('#heightFld'),
        minesFld = $('#minesFld'),
        game = new Game(field, 20);

    $('#newGameBtn').on('click', function () {
        var width = parseInt(widthFld.val()),
            height = parseInt(heightFld.val()),
            minesCount = Math.min(parseInt(minesFld.val()), width * height - 1);

        minesFld.val(minesCount);

        game.newGame(width, height, minesCount);
    });

    field.on(Game.EVENT_LOSE, function () {
        alert('You are dead!');
    });

    field.on(Game.EVENT_WIN, function () {
        alert('Victory!');
    });
});
